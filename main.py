import datetime as dt
import random as r
import smtplib
import pandas as p
import os


my_email = "tofnraid1@gmail.com"
password = os.environ['PASS']

# Data csv module
data = p.read_csv('birthdays.csv')

# Datetime
now = dt.datetime.now()

# Data to list filled with dictionaries
data_dict = data.to_dict('records')


# Function to select a random letter
def rand_letter():
    number = r.randint(0, 2)
    if number == 0:
        with open("letter_templates/letter_1.txt") as read_l:
            letter = read_l.read().replace('[NAME]', f"{d['name']}")
            return letter
    elif number == 1:
        with open("letter_templates/letter_2.txt") as read_l:
            letter = read_l.read().replace('[NAME]', f"{d['name']}")
            return letter
    elif number == 2:
        with open("letter_templates/letter_3.txt") as read_l:
            letter = read_l.read().replace('[NAME]', f"{d['name']}")
            return letter


# Sending a letter to person with birthday
for d in data_dict:
    if d['day'] == now.day and d['month'] == now.month:
        with smtplib.SMTP("smtp.gmail.com", port=587) as connection:
            connection.starttls()
            connection.login(user=my_email, password=password)
            connection.sendmail(
                from_addr=my_email,
                to_addrs=f"{d['email']}",
                msg=f"Subject:Happy Birthday!\n\n{rand_letter()}")



